#! /usr/bin/env python3

from subprocess import Popen, PIPE
import time
import struct
import user
import os
import conf


def check_call(func_name, t=None, mode=None):
    if hasattr(user, func_name):
        func = getattr(user, func_name)
        if hasattr(func, '__call__'):
            new_mode = func(t, mode)
            if new_mode:
                return new_mode
        else:
            print("Function '{}' not defined!".format(func_name))
    else:
        print("Function '{}' not defined!".format(func_name))
    return mode
        

os.setuid(conf.USER)
f = open(conf.PATH, 'rb')

CURR = conf.MODES[0]
open('mode', 'w').write(CURR)

byte = f.read(1)

l1 = []
l2 = []
t = [0, 0, 0] # elapsed time between press/release for each button
while byte != b'':
    if len(l1) == 24:
        if len(l2) == 24:
            l1 = l2
            l2 = []

        l2.append(struct.unpack('B', byte)[0])
    else:
        l1.append(struct.unpack('B', byte)[0])

    if len(l1) == len(l2): # 24
        # Press
        if l1[-4] == 181 and l2[-4] == 1: # G1 Press
            print("G1 pressed!")
            t[0] = time.time()
        
        elif l1[-4] == 205 and l2[-4] == 1: # G2 Press
            print("G2 pressed!")
            t[1] = time.time()

        elif l1[-4] == 182 and l2[-4] == 1: # G3 Press
            print("G3 pressed!")
            t[2] = time.time()

        # Release
        elif l1[-4] == 181 and l2[-4] == 0: # G1 Release
            print("G1 released!")
            CURR = check_call("g1_press", time.time() - t[0], CURR)
        
        elif l1[-4] == 205 and l2[-4] == 0: # G2 Release
            print("G2 released!")
            CURR = check_call("g2_press", time.time() - t[1], CURR)

        elif l1[-4] == 182 and l2[-4] == 0: # G3 Release
            print("G3 released!")
            CURR = check_call("g3_press", time.time() - t[2], CURR)

        # Whell scroll
        elif l1[-4] == 233 and l2[-4] == 0: # Up Scroll
            # print("Volume up!")
            check_call("up_scroll")

        elif l1[-4] == 234 and l2[-4] == 0: # Down Scroll
            # print("Volume down!")
            check_call("down_scroll")
    
    byte = f.read(1)
