This script is intended to handle the Logitech G930 Headset on Linux.
The Gx buttons and the whell aren't supported by default.

I personally use the Gx buttons for music purposes. I also use clementine as
a music player. The commands are therefore made for this program. It is however
really easy to modify the actions that are made after a button is pushed.

Execute the main script file as root with python3: headset.py

There are 2 files to modify, some lines have to be modified whereas others are 
optional.

# Required

## User ID

file: conf.py

The script needs to change to your User when executing commands. Otherwise they
would be executed as root and the output wouldn't be the one expected.
To get your User ID :

    python
    >>> import os
    >>> os.getuid()
    1000

## Path of event

file: conf.py

I don't know if there is a better way to do this, but to get the file where
your headset writes, do:
    
    cat /dev/input/event0
    cat /dev/input/event1
    ...

until you find a file that writes something on your terminal when pushing a
button on your headset.

## Soundcard

file: conf.py

To get the number of your soundcard, open alsamixer and press F6. You'll see a
list of soundcards, just remember the number before "Logitech G930 Headset".

# Optional

## Modes

file: conf.py

If you want to have multiples modes with your buttons, just add their name.
You will be able to use them later.

## Press Time

file: conf.py

By default, you'll have to press the G3 buttons for 0.4 seconds to change to
the next mode.

## Actions

file: user.py

Here you can define what each button press of the headset does.
By default the G1, G2 and G3 buttons respectivly send 'next', 'play/pause' and
'previous' to clementine.
On the "vlc" mode, the G2 button sends 'play/pause' to VLC through its web
interface.

The G3 button is used to change mode. If you want to change this button for
another one, just cut the 3 first lines of the function and paste them in
another one. If you want to write it in another way, remember to return the
new mode!

The "t" argument given to the function is the elapsed time between the press
and the release of the button. The "mode" one is the current mode.


