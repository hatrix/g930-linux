from subprocess import Popen, PIPE
from urllib.request import urlopen
import conf

def g1_press(t, mode):
    if mode == 'clementine':
        p = Popen(['clementine', '-f'], stdout=PIPE, stderr=PIPE)

def g2_press(t, mode):
    if mode == 'clementine':
        p = Popen(['clementine', '-t'], stdout=PIPE, stderr=PIPE)
    
    elif mode == 'vlc':
        host = 'http://localhost:8080'
        urlopen(host + "/requests/status.xml?command=pl_pause&id=0")
    
def g3_press(t, mode):
    changed, new = change_mode(t, mode)
    if changed:
        return new

    if mode == 'clementine':
        p = Popen(['clementine', '-r'], stdout=PIPE, stderr=PIPE)

def up_scroll(*args):
    change_vol(1)

def down_scroll(*args):
    change_vol(-1)


def change_mode(t, mode):
    if t >= conf.PRESS_TIME:
        print("Passed from mode '{}' to ".format(mode), end="")
        curr = conf.MODES[(conf.MODES.index(mode) + 1) 
                           % len(conf.MODES)]
        print("'{}'".format(curr))
        open('mode', 'w').write(curr)
        
        return 1, curr
    return 0, ''

def change_vol(updown):
    if updown < 0:
        db = '1dB-'
        msg = "lowered"
    else:
        db = '1dB+'
        msg = "upped"

    p1 = Popen(['amixer', '-c', conf.CARD, 'set', 'PCM', db],
              stdout=PIPE,
              stderr=PIPE)
    p2 = Popen(['egrep', '-o', '[0-9]+%'], 
               stdin=p1.stdout, 
               stdout=PIPE,
               stderr=PIPE)
    percent = (p2.communicate()[0].strip()).decode()

    print("Volume {} to {}".format(msg, percent))
